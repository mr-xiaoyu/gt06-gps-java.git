package club.mrxiao.gps.common.properties;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


/**
 * 线程池配置
 * @author xiaoyu
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "gps.thread")
public class ThreadProperties {
    /**
     * 核心线程数
     */
    private int corePoolSize;

    /**
     * 最大线程数
     */
    private int maxPoolSize;

    /**
     * 队列大小
     */
    private int queueCapacity;

    /**
     * 线程池中的线程的名称前缀
     */
    private String namePrefix;
}
