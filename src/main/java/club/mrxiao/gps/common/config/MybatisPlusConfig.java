package club.mrxiao.gps.common.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * MybatisPlus配置
 * @author xiaoyu
 */
@Configuration
@MapperScan(value = {"club.mrxiao.gps.dao"})
public class MybatisPlusConfig {

}