package club.mrxiao.gps.common.listener;

import club.mrxiao.gps.common.properties.GpsProperties;
import club.mrxiao.gps.common.utils.SpringContextUtil;
import club.mrxiao.gps.socket.GpsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * 项目启动监听器
 * @author xiaoyu
 */

@Slf4j
public class StartListener implements ApplicationListener<ContextRefreshedEvent> {
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        log.info("项目启动");
    }
}
