package club.mrxiao.gps.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 终端
 * @author xiaoyu
 */
@Data
@TableName("t_entity")
public class Entity {

    /**
     * ID
     */
    @TableId(value = "entity_id",type = IdType.AUTO)
    private Integer entityId;

    /**
     * 名称
     */
    private String entityName;

    /**
     * IMEI码
     */
    private String entityImei;

    /**
     * 创建人
     */
    private Integer creationUser;

    /**
     * 创建时间
     */
    private Date creationTime;
}
