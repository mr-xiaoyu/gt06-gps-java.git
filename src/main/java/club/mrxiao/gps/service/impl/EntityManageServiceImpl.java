package club.mrxiao.gps.service.impl;

import club.mrxiao.baidu.api.BaiduTraceService;
import club.mrxiao.baidu.exception.BaiduTraceException;
import club.mrxiao.baidu.request.BaiduTraceCommonRequest;
import club.mrxiao.baidu.request.BaiduTraceEntityRequest;
import club.mrxiao.baidu.response.BaiduTraceEntityListResponse;
import club.mrxiao.gps.service.EntityManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 轨迹管理服务相关接口实现
 * @author xiaoyu
 */
@Service
public class EntityManageServiceImpl implements EntityManageService {

    private final BaiduTraceService baiduTraceService;

    @Autowired
    public EntityManageServiceImpl(BaiduTraceService baiduTraceService) {
        this.baiduTraceService = baiduTraceService;
    }

    @Override
    public void creationBaiduEntity(BaiduTraceEntityRequest baiduTraceEntityRequest) throws BaiduTraceException {
        this.baiduTraceService.getEntityService().entityAdd(baiduTraceEntityRequest);
    }

    @Override
    public void updateBaiduEntity(BaiduTraceEntityRequest baiduTraceEntityRequest) throws BaiduTraceException {
        this.baiduTraceService.getEntityService().entityUpdate(baiduTraceEntityRequest);
    }

    @Override
    public void deleteBaiduEntity(BaiduTraceEntityRequest baiduTraceEntityRequest) throws BaiduTraceException {
        this.baiduTraceService.getEntityService().entityDelete(baiduTraceEntityRequest);
    }

    @Override
    public BaiduTraceEntityListResponse findBaiduTraceEntityList(BaiduTraceCommonRequest baiduTraceCommonRequest) throws BaiduTraceException {
        return this.baiduTraceService.getEntityService().entityList(baiduTraceCommonRequest);
    }
}
