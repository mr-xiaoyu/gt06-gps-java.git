package club.mrxiao.gps.service.impl;

import club.mrxiao.baidu.exception.BaiduTraceException;
import club.mrxiao.baidu.request.BaiduTraceEntityRequest;
import club.mrxiao.gps.dao.EntityMapper;
import club.mrxiao.gps.domain.Entity;
import club.mrxiao.gps.service.EntityManageService;
import club.mrxiao.gps.service.EntityService;
import club.mrxiao.gps.service.TrackManageService;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 终端服务相关接口
 * @author xiaoyu
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class EntityServiceImpl extends ServiceImpl<EntityMapper,Entity> implements EntityService {

    private final EntityManageService entityManageService;

    @Autowired
    public EntityServiceImpl(EntityManageService entityManageService) {
        this.entityManageService = entityManageService;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void creationEntity(Entity entity) throws BaiduTraceException {
        entity.setCreationTime(new Date());
        this.save(entity);
        BaiduTraceEntityRequest request = new BaiduTraceEntityRequest();
        request.entityName(entity.getEntityName());
        entityManageService.creationBaiduEntity(request);
    }

    @Override
    public Entity findByImei(String imei) {
        if(StrUtil.isBlank(imei)){
            return null;
        }
        LambdaQueryWrapper<Entity> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(Entity::getEntityImei,imei);
        return this.baseMapper.selectOne(lambdaQueryWrapper);
    }
}
