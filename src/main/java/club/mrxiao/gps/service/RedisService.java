package club.mrxiao.gps.service;

import club.mrxiao.gps.exception.RedisConnectException;

import java.util.Set;

/**
 * redis服务接口
 * @author xiaoyu
 */
public interface RedisService {

    /**
     * get命令
     * @param key
     * @return
     * @throws RedisConnectException Redis异常
     */
    String get(String key) throws RedisConnectException;

    /**
     * set命令
     * @param key
     * @param value
     * @return
     * @throws RedisConnectException Redis异常
     */
    void set(String key, String value) throws RedisConnectException;

    /**
     * set 命令
     * @param key
     * @param value
     * @param milliscends 毫秒
     * @return
     * @throws RedisConnectException Redis异常
     */
    void set(String key, String value, Long milliscends) throws RedisConnectException;

    /**
     * del命令
     * @param key
     * @return
     * @throws RedisConnectException Redis异常
     */
    void del(String... key) throws RedisConnectException;

    /**
     * exists命令
     * @param key
     * @return
     * @throws RedisConnectException Redis异常
     */
    Boolean exists(String key) throws RedisConnectException;

    /**
     * zadd 命令
     * @param key
     * @param score
     * @param member
     * @return
     * @throws RedisConnectException Redis异常
     */
    void zadd(String key, Double score, String member) throws RedisConnectException;


    /**
     * zrange 命令
     * @param key
     * @param min
     * @param max
     * @return
     * @throws RedisConnectException Redis异常
     */
    Set<String> zrange(String key, long min, long max) throws RedisConnectException;


    /**
     * zrem 命令
     * @param key
     * @param members
     * @return
     * @throws RedisConnectException Redis异常
     */
    void zrem(String key, String... members) throws RedisConnectException;
}
