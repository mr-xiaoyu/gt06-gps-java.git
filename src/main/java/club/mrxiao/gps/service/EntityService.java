package club.mrxiao.gps.service;

import club.mrxiao.baidu.exception.BaiduTraceException;
import club.mrxiao.gps.domain.Entity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 终端服务相关接口
 * @author xiaoyu
 */
public interface EntityService extends IService<Entity> {

    /**
     * 创建终端
     * @param entity
     * @throws BaiduTraceException
     */
    void creationEntity(Entity entity) throws BaiduTraceException;

    /**
     * 通过Imei码查询
     * @param imei
     * @return
     */
    Entity findByImei(String imei);
}
