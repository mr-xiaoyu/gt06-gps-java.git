package club.mrxiao.gps.socket.domain;

import club.mrxiao.gps.socket.util.GpsDataUtil;
import lombok.Data;

import java.io.Serializable;

/**
 * 设备状态数据解析
 * @author  xiaoyu
 *
 */
@Data
public class GpsStateInputDto implements Serializable {

    private static final long serialVersionUID = -3188497383799796186L;
    /**
     * 是否设防
     */
    private String isFortify;

    /**
     * ACC状态
     */
    private String accState;

    /**
     * 电源状态
     */
    private String powerState;

    /**
     * 警报
     */
    private String alarm;

    /**
     * GPS状态
     */
    private String gpsState;

    /**
     * 油电状态
     */
    private String petrolElectricState;

    /**
     * 电压等级
     */
    private String voltage;

    /**
     * 信号强度
     */
    private String signalIntensity;

    /**
     * 报警语言
     */
    private String language;

    /**
     * 原始数据
     */
    private String originalData;

    /**
     * 路线名
     */
    private String name;
    /**
     * 设备ID
     */
    private Integer entityId;
    /**
     * 设备IMEI码
     */
    private String imei;
    public GpsStateInputDto(String input,String name,Integer entityId,String imei){
        this.originalData = input;
        this.name = name;
        this.entityId = entityId;
        this.imei = imei;
        this.parseGpsState(input);
    }

    private void parseGpsState(String input){
        String state = GpsDataUtil.bytes2BinaryStr(GpsDataUtil.parseHexStr2Byte(input.substring(0, 2)));
        input = input.substring(2);
        this.isFortify = state.substring(0,1);
        state = state.substring(1);
        this.accState = state.substring(0,1);
        state = state.substring(1);
        this.powerState = state.substring(0,1);
        state = state.substring(1);
        this.alarm = state.substring(0,3);
        state = state.substring(3);
        this.gpsState = state.substring(0,1);
        state = state.substring(1);
        this.petrolElectricState = state.substring(0,1);
        this.voltage = input.substring(0,2);
        input = input.substring(2);
        this.signalIntensity = input.substring(0,2);
        input = input.substring(2);
        this.language = input.substring(0,4);
    }
}
